
AHAH Edit Node In Place
==========================

Provides both the API and UI to enable in-place editing of node fields. Currently, UI only supports CCK and the node body, but the API may be used
in combination with the theming system to enable editing of virtually any node field.

The module works by only fetching the relevant parts of the Node Form for each edited field (or field group), via AHAH (hence the name), and takes care of reloading only the changed data once the form has been submitted (also via AHAH).

Installation:
1) Enable the API and the UI modules
2) Apply this patch: http://drupal.org/node/335741. This is because when loading the node form via AHAH, the module loads and applies any javascript included in the form fragment, which causes havoc due to a bug in Drupal.extend() this patch resolves. 

Module development sponsored by ewave.co.il




shai dot yallin at gmail.com