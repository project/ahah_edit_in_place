
(function($) 
{
	/**
	 * Adds a node edit form inside the specified container
	 */
	$.fn.editNodeInPlace = function()
	{
		return this.each(function() 
		{
			var container = $(this);
			
			function init()
			{
				var settings = Drupal.settings.ahahEdit.editContainers[container.attr('id')];
				
				if (settings)
				{
					// make sure we don't have duplicate event handlers
					container.unbind('submit');		
					
					container.bind('submit', function(event, response)
					{
						containerSubmitted(container, response, settings);
					});		
					
					// temporary solution
					container.bind('save cancel', function()
					{
						container.addEditLinkBehavior();
					});							
					
					loadEditForm(container, settings.nid, settings.fields, settings.title);
				}
			}
		
			/**
			 * Loads a single node's partial edit form
			 */
			function loadEditForm(container, nid, fields, title)
			{
				// save the original html content
				container.oldHtml = container.html();
				
				var url = Drupal.settings.jstools.basePath + Drupal.settings.ahahEdit.editPath + '/' + nid;
				
				params = {'edit_in_place_fields': fields}
				
				if (title)
				{
					params.title = title;
				}	
				
				container.addClass('editing');				
				
				container.loadAndProcessScripts(url, params, function()
				{
					prepareEditForm(container);					
				});		
			}		
			
			// "main"
			init();
		});		
	} // end of editNodeInPlace
		
	/**
	 * Temporary, will be removed
	 */
	$.fn.addEditLinkBehavior = function()
	{
		return this.each(function()
		{
			var container = $(this);
			
			container.css('position', 'relative');
			
			var editButton = container.find('a.edit-button, a.edit-button-empty-content');
			
			if (!editButton.hasClass('edit-button-empty-content'))
			{
				editButton.hide();
				
				container.hover
				(
					function()
					{
						editButton.show();
					},
					function()
					{
						editButton.hide();
					}
				)			
			}
			
			// make sure we don't have duplicate event handlers
			editButton.unbind('click');
			
			editButton.click(function()
			{
				container.editNodeInPlace();
				
				return false;
			});			
		});
	}
	
	/**
	 * Adds a node create form inside the specified object
	 */
	$.fn.createNodeInPlace = function(params) 
	{
		var editBoxClass = 'create-node-reference-wrapper';
		
		return this.each(function() 
		{
			var container = $(this);
			
			if (container.find('.' + editBoxClass).length == 0)
			{
				var options = {append: false};
				
				$.extend(options, params);
				
				var settings = Drupal.settings.ahahEdit.createContainers[container.attr('id')];
				
				if (settings)
				{				
					loadCreateForm(container, settings.parentNodeId, settings.referenceFieldName, settings.type, settings.fields, settings.title, options.append);
					
					// make sure we don't have duplicate event handlers
					container.unbind('submit');								
					
					container.bind('submit', function(event, response)
					{
						containerSubmitted(container, response, settings);
					});
				}	
			}

			/**
			 * Loads a create form for a referenced node
			 */
			function loadCreateForm(container, parentNodeId, referenceFieldName, type, fields, title, append)
			{
				// save the original html content
				container.oldHtml = container.html();
				
				//container.addClass('editing');
				
				var url = Drupal.settings.jstools.basePath + Drupal.settings.ahahEdit.createPath + '/' + type + '/' + parentNodeId + '/' + referenceFieldName;
				
				var editBox = '<div class="' + editBoxClass + ' editing"></div>';
				
				if (append)
				{
					container.append(editBox);
				}
				else
				{
					container.prepend(editBox);
				}
				
				params = {'edit_in_place_fields': fields}
				
				if (title)
				{
					params.title = title;
				}
				
				container.find('div.create-node-reference-wrapper').loadAndProcessScripts(url, params, function()
				{
					prepareEditForm(container);
				});			
			}			

		});

	}; // end of createNodeInPlace
	
	/**
	 * Reloads a container using ajax
	 */
	$.fn.ajaxReload = function(callback)
	{
		return this.each(function() 
		{
			callback = callback || function(){};
			
			var container = $(this);	
			
			// if container has no id, add a temp one
			
			var id = container.attr('id');
			
			if (!id)
			{
				id = 'container' + Math.round(Math.random() * 100000);
				
				container.attr('id', id);
			}
			
			container.load(document.location.href + ' #' + id, null, function()
			{
				// after load, we have two identical DIV elements one inside the other
				// we have to remove the inner one
				var html = container.find('#' + id).html();
				
				container.empty().html(html);
				
				callback();
			});	
		});
	} // end of ajaxReload
	
	$.fn.loadMultiple = function(url, callback)
	{
		var self = this;
		
		callback = callback || function(){};
		
		$.post
		(
			url,
			function(res, status)
			{
				// If successful, inject the HTML into all the matched elements
				if ( status == "success" || status == "notmodified" )
				{
					var response = jQuery(res); // .replace(/<script(.|\s)*?\/script>/g, "")
					
					// refresh all selectors
					self.each(function() 
					{		
						// go up the DOM, find the first parent with an ID
						var container = $(this);
						
						if (container.length > 0)
						{
							while (!container.attr('id') && container.attr('tagName') != 'body')
							{
								container = container.parent();
							}
							
							container.empty().html(response.find('#' + container.attr('id')).html());
						}
						else
						{
							alert('loadMultiple requested to reload invalid selector ' + this);
						}
					});
										
					callback();
				}
			}
		);
		
		return this;
	}
	
	/**
	 * Removes a referenced node from a parent node
	 */
	$.fn.removeReferenceInPlace = function() 
	{
		return this.each(function() 
		{
			var container = $(this);
			
			var settings = Drupal.settings.ahahEdit.removableContainers[container.attr('id')];
			
			if (settings)
			{
				var url = Drupal.settings.jstools.basePath 
						+ Drupal.settings.ahahEdit.removePath 
						+ '/' + settings.parentNodeId 
						+ '/' + settings.referenceFieldName
						+ '/' + settings.referencedNodeId
						+ '/' + settings.deleteReferenced
				
				$.get(url, function(data, textStatus)
				{
					if (textStatus == 'success')
					{
						var parentContainer = $('#' + settings.parentContainerId);
						
						parentContainer.ajaxReload(function()
						{
							parentContainer.trigger('remove');
						});						
					}
				});
			}			
		});
	} // end of removeReferenceInPlace
	
	/**
	 * Reimplementation of AJAX load to filter existing scripts
	 */
	$.fn.loadAndProcessScripts = function(url, params, callback)
	{

		var off = url.indexOf(" ");
		if ( off >= 0 ) {
			var selector = url.slice(off, url.length);
			url = url.slice(0, off);
		}

		callback = callback || function(){};

		// Default to a GET request
		var type = "GET";

		// If the second parameter was provided
		if ( params )
			// If it's a function
			if ( jQuery.isFunction( params ) ) {
				// We assume that it's the callback
				callback = params;
				params = null;

			// Otherwise, build a param string
			} else {
				params = jQuery.param( params );
				type = "POST";
			}

		var self = this;

		// Request the remote document
		$.ajax({
			url: url,
			type: type,
			dataType: "html",
			data: params,
			complete: function(res, status){
				// If successful, inject the HTML into all the matched elements
				if ( status == "success" || status == "notmodified" )
				{
					// See if a selector was specified
					var newContent = jQuery( selector ?
						// Create a dummy div to hold the results
						$("<div/>")
							// inject the contents of the document in, removing the scripts
							// to avoid any 'Permission Denied' errors in IE
							.append(res.responseText.replace(/<script(.|\s)*?\/script>/g, ""))

							// Locate the specified elements
							.find(selector) :

						// If not, just inject the full result
						res.responseText );
				
					var removed = [];
					
					// remove all existing scripts from the new content
					$('script[src]').each(function(i, script)
					{
						var src = script.src.toLowerCase().substring(script.src.lastIndexOf('/'));

						var removedScript = newContent.find('script[src*=\'' + src + '\']');
						
						if (removedScript.length > 0)
						{
							removed.push(removedScript[0]);
						}
						
						removedScript.remove();
					});
					
					//window.console.log('removed: ', removed);
					//window.console.log('injecting: ', newContent.find('script[src]'));
					
					try
					{
						self.html(newContent);
					}
					catch (e)
					{
						
					}

				}

				self.each( callback, [res.responseText, status, res] );
			}
		});
		return this;		
	}	
	
	// private functions

	/**
	 * Prepares the create or edit form for AJAX submission
	 */
	function prepareEditForm(container)
	{
		// add 'cancel' operation
		container.find('a.cancel-button').click(function()
		{
			cancelEdit(container);
			
			container.removeClass('editing');
			
			return false;
		});
		
		// make sure we don't have duplicate event handlers
		container.find('a.save-button').unbind('click');
		
		// add 'save' operation
		container.find('a.save-button').click(function()
		{
			container.find('form').submit();
			
			return false;
		});	
		
		// redirect submit operation to use our serializing function
		container.find('form').submit(function()
		{
			return ajaxSubmit(this, container);
		});
		
		// call autocomplete to attach fields if any exist
		if (typeof(Drupal.autocompleteAutoAttach) == 'function')
		{
			Drupal.autocompleteAutoAttach();
		}
			
		// initialize Hierarchical Selects
		if (typeof(Drupal.HierarchicalSelect) != 'undefined' && 
			typeof(Drupal.HierarchicalSelect.initialize) == 'function')
		{
			Drupal.HierarchicalSelect.initialize();
		}
		
		// initialize maps
		if (typeof(Drupal.gmap) != 'undefined')
		{
			Drupal.gmap.setup();
		}
		
		// bind error handler
		container.bind('error', formError);
		
		// only remove this class once we've saved the data
		container.bind('save', function()
		{
			container.removeClass('editing');
		});
		
		// all done, trigger callbacks
		container.trigger('load');
	}
	

	/**
	 * Restores the original static content 
	 */
	function cancelEdit(container)
	{
		container.html(container.oldHtml);
		
		container.trigger('cancel');
	}

	/**
	 * Serializes this form and submits it using AJAX
	 */
	function ajaxSubmit(form, container)
	{
			
		var values = $(form).serializeArray();
		
		values.push({name: '#container_id', value: container.attr('id')});
		
		$.post(	form.action, values, function(data)
		{	
			if (data.status == 'ok')
			{	
				container.trigger('submit', data);				
			}
			else
			{
				container.trigger('error', data.errors);
			}				
		}, 'json');
		
		return false;
	}
	
	/**
	 * Adds error messages to forms
	 */
	function formError(event, errors)
	{
		// remove old errors, if any
		$(this).find('span.form-error').remove();
		
		for (field in errors)
		{
			$('#edit-' + field).after('<span class="form-error">' + errors[field] + '</span>');
		}			
	}
	
	/**
	 * Called after an edit form has successfully submitted
	 */
	function containerSubmitted(container, response, settings)
	{
		var reload = (typeof(settings.reload) != 'undefined' && settings.reload && settings.reload.length) ? settings.reload : null;
		var isNodePage = (typeof(settings.isNodePage) == 'undefined' ? false : settings.isNodePage);
		
		// if response.redirect differs from current path name and this is a node
		// edit page (as opposed to, for instance, a gallery page), the location
		// of the edited name has changed, redirect instead of ajax reload
		if (isNodePage && document.location.pathname != response.redirect)
		{
			window.location.href = response.redirect;
		}
		else
		{
			var target = reload ? $(reload) : container;
			var url = (response.redirect && isNodePage) ? response.redirect : document.location.pathname;

			target.loadMultiple(url, function()
			{
				container.trigger('save', response);
			});
		}
	}	
	
})(jQuery); 
